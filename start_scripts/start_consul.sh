#!/bin/sh

consul agent -server -config-dir CONSUL_CONFIG_DIR -datacenter local -ui -bind BIND_ADDRESS -client BIND_ADDRESS -bootstrap-expect 1
