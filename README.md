# local-consul-vault

A small local testsetup using Consul and Vault.

## Installation
Run ./install.sh

This will:

* install binaries into $HOME/bin
* create config directories $HOME/.consul.d and $HOME/.vault.d
* create 2 start scripts in $HOME/bin

Currenty the following versions are installed:

* CONSUL_VERSION="1.5.1"
* VAULT_VERSION="1.1.3"

Adjust these variables in install.sh to get newer versions.


## Run

### start_consul

Start Consul as follows in it's own terminal.

When you keep it running in it's own terminal it's easier to follow the messages as they come along when you are experimenting with Consul and Vault commands.

```
$ start_consul.sh
BootstrapExpect is set to 1; this is the same as Bootstrap mode.
bootstrap = true: do not enable unless necessary
==> Starting Consul agent...
==> Consul agent running!
           Version: 'v1.5.1'
           Node ID: 'c27ae459-8fc8-5059-d4ae-981edcee1487'
         Node name: 'yourhostname.localdomain'
        Datacenter: 'local' (Segment: '<all>')
            Server: true (Bootstrap: true)
       Client Addr: [127.0.0.1] (HTTP: 8500, HTTPS: -1, gRPC: -1, DNS: 8600)
      Cluster Addr: 127.0.0.1 (LAN: 8301, WAN: 8302)
           Encrypt: Gossip: false, TLS-Outgoing: false, TLS-Incoming: false

==> Log data will now stream in as it occurs:

    2019/06/27 11:33:08 [WARN] agent: Node name "yourhostname.localdomain" will not be discoverable via DNS due to invalid characters. Valid characters include all alpha-numerics and dashes.
    2019/06/27 11:33:08 [INFO] raft: Initial configuration (index=1): [{Suffrage:Voter ID:c27ae459-8fc8-5059-d4ae-981edcee1487 Address:127.0.0.1:8300}]
(...)
```


### start_vault

Start Vault as follows in it's own terminal.

```
$ start_vault.sh
==> Vault server configuration:

             Api Address: http://127.0.0.1:8200
                     Cgo: disabled
         Cluster Address: https://127.0.0.1:8201
              Listener 1: tcp (addr: "0.0.0.0:8200", cluster address: "0.0.0.0:8201", max_request_duration: "1m30s", max_request_size: "33554432", tls: "disabled")
               Log Level: info
                   Mlock: supported: true, enabled: false
                 Storage: consul (HA available)
                 Version: Vault v1.1.3
             Version Sha: 9bc820f700f83a7c4bcab54c5323735a581b34eb

==> Vault server started! Log data will stream in below:

2019-06-27T11:35:58.080+0200 [WARN]  storage.consul: appending trailing forward slash to path
2019-06-27T11:35:58.085+0200 [WARN]  no `api_addr` value specified in config or in VAULT_API_ADDR; falling back to detection if possible, but this value should be manually set
(...)
```
