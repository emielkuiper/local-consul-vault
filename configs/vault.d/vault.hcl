disable_cache = true
disable_mlock = true
ui = true

storage "consul" {
  address = "BIND_ADDRESS:8500"
  path = "vault"
  scheme = "http"
}

listener "tcp" {
  address = "0.0.0.0:8200"
  tls_disable = 1
}
