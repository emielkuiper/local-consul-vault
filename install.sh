#!/bin/bash

###
### Setting variables
###

export OSTYPE=`uname`
export CONSUL_VERSION="1.5.2"
export VAULT_VERSION="1.1.3"
export CONSUL_TMPFILE="/tmp/consul.zip"
export VAULT_TMPFILE="/tmp/vault.zip"
export CONSUL_CONFIG_DIR="$HOME/.consul.d"
export VAULT_CONFIG_DIR="$HOME/.vault.d"
export CONSUL_DATA_DIR="$HOME/consul_data"
export INSTALL_PREFIX="$HOME/bin"



###
### Functions
###

function determine_listen_address {

if   [[ "$OSTYPE" =~ "Darwin" ]]; then
  echo ""
  echo "OS type determined as $OSTYPE"
  export LOCAL_IF=`netstat -rn | grep ^default | grep -v tun | awk '{print $NF}' | sort | head -n1`
  export LOCAL_IP=`ifconfig $LOCAL_IF | grep inet\ | awk '{print $2}'`
  echo "Working with interface: $LOCAL_IF"
  echo "Working with IP address: $LOCAL_IP"
  echo ""
elif [[ "$OSTYPE" =~ "Linux" ]]; then
  echo ""
  echo "OS type determined as $OSTYPE"
  export LOCAL_IF=`ip route | grep default | cut -d' '  -f5`
  export LOCAL_IP=`ip -f inet addr show dev $LOCAL_IF | grep inet | awk '{print $2}' | cut -d'/' -f1`
  echo "Working with interface: $LOCAL_IF"
  echo "Working with IP address: $LOCAL_IP"
  echo ""
fi
}

function get_binary_consul {
echo ""
echo "############## Download - Consul binary ##################"
echo ""
echo "Downloading Consul binary to $CONSUL_TMPFILE"
echo ""

if   [[ "$OSTYPE" =~ "Darwin" ]]; then
  echo ""
  curl --progress-bar -SL -o $CONSUL_TMPFILE "https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_darwin_amd64.zip"
  echo ""
elif [[ "$OSTYPE" =~ "Linux" ]]; then
  echo ""
  curl --progress-bar -SL -o $CONSUL_TMPFILE "https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip"
  echo ""
fi
echo ""
echo ""
}


function get_binary_vault {
echo ""
echo "############## Download - Vault binary ##################"
echo ""
echo "Downloading Vault binary to $VAULT_TMPFILE"
echo ""

if   [[ "$OSTYPE" =~ "Darwin" ]]; then
  echo ""
  curl --progress-bar -SL -o $VAULT_TMPFILE "https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_darwin_amd64.zip"
  echo ""
elif [[ "$OSTYPE" =~ "Linux" ]]; then
  echo ""
  curl --progress-bar -SL -o $VAULT_TMPFILE "https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip"
  echo ""
fi
echo ""
echo ""
}

function create_config_and_data_dirs {
if [[ ! -e $CONSUL_CONFIG_DIR ]]; then
    echo "Creating Consul config dir: $CONSUL_CONFIG_DIR "
    mkdir $CONSUL_CONFIG_DIR
else
    echo "Not creating: $CONSUL_CONFIG_DIR already exists."
fi

if [[ ! -e $CONSUL_DATA_DIR ]]; then
    echo "Creating Consul data dir: $CONSUL_DATA_DIR "
    mkdir $CONSUL_DATA_DIR
else
    echo "Not creating: $CONSUL_DATA_DIR already exists."
fi

if [[ ! -e $VAULT_CONFIG_DIR ]]; then
    echo "Creating Vault config dir: $VAULT_CONFIG_DIR "
    mkdir $VAULT_CONFIG_DIR
else
    echo "Not creating: $VAULT_CONFIG_DIR already exists."
fi
}

function install_binaries {
  unzip -od $INSTALL_PREFIX $CONSUL_TMPFILE
  unzip -od $INSTALL_PREFIX $VAULT_TMPFILE
  rm $CONSUL_TMPFILE
  rm $VAULT_TMPFILE
  chmod 755 $INSTALL_PREFIX/consul
  chmod 755 $INSTALL_PREFIX/vault
}


function create_config_files {
  cat configs/consul.d/consul.json | sed "s^CONSUL_DATA_DIR^$CONSUL_DATA_DIR^g" > $CONSUL_CONFIG_DIR/consul.json
  cat configs/consul.d/consul_config.json > $CONSUL_CONFIG_DIR/consul_config.json
  cat configs/vault.d/vault.hcl | sed "s^BIND_ADDRESS^$LOCAL_IP^g" > $VAULT_CONFIG_DIR/vault.hcl

  chmod 755 $CONSUL_CONFIG_DIR/consul.json
  chmod 755 $CONSUL_CONFIG_DIR/consul_config.json
  chmod 755 $VAULT_CONFIG_DIR/vault.hcl
}

function install_start_scripts {

  echo "Installing start scripts"
  cat start_scripts/start_consul.sh | sed "s^CONSUL_CONFIG_DIR^$CONSUL_CONFIG_DIR^g"  | sed "s^BIND_ADDRESS^$LOCAL_IP^g" > $INSTALL_PREFIX/start_consul.sh
  cat start_scripts/start_vault.sh | sed "s^VAULT_CONFIG_DIR^$VAULT_CONFIG_DIR^g" > $INSTALL_PREFIX/start_vault.sh

  chmod 755 $INSTALL_PREFIX/start_consul.sh
  chmod 755 $INSTALL_PREFIX/start_vault.sh
}


### Main
get_binary_consul
get_binary_vault
determine_listen_address
create_config_and_data_dirs
install_binaries
create_config_files
install_start_scripts

echo ""
echo "Everything should be ready to start using the scripts: start_consul.sh and start_vault.sh"
echo ""
echo "Processes will listen on:"
echo "- Consul : $LOCAL_IP:8500"
echo "- Vault  : 0.0.0.0:8200"
echo ""
echo "Assuming that ~/bin is in your PATH though!"
echo "If not: please add ~/bin to your PATH"
